from flask import Flask, jsonify, render_template, request
import pymysql

app = Flask(__name__)

conn = pymysql.connect(host='localhost', user='root', password='jong',
                               db='iotdb', charset='utf8')

curs = conn.cursor()

@app.route("/")
def main():
    return render_template('main.html')

@app.route("/temperature")
def temperature():
    btn_attr = request.args.get('status')  
    if btn_attr == None:
        return render_template('Temperature.html')

    if btn_attr == 'on':
        return 'off'
    else:    
        return 'on'

'''
@app.route("/test")
def test():
    sql = "select * from test"
    curs.execute(sql)

    rows = curs.fetchall()
    return str(rows)
'''


if __name__ == '__main__':
    app.run(debug=True)
